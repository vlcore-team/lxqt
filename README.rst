LXQT SUBMODULE
===============

This repository holds the build data for all components of the
lxqt desktop for Vector and VLocity linux.  The changes from
one release to another are tracked in different branches of this
repository.


HOW THIS REPO WORKS
====================
* *The master branch must remain empty.*  Nothing from the master
  branch is built by the buildbot.

* The buildbot will update to veclinux-x.x on pull
* src/lxqt.SlackBuild should trigger the build of all packages and
  generate the lxqt meta package including default desktop configuration
  files specific for the intended release.



